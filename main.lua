
--Fish fillets remake
--By glitchapp
--Ported from the löve2d game

WIDTH = 32
HEIGHT = 32
LEN = 5
dx=0
dy=0

function love.conf(t)
	t.width  = WIDTH*12
	t.height = HEIGHT*12
end

function love.load()

nLevel = 1
level = loadlevel()	-- load level

	fish3x1 = love.graphics.newImage("/assets/fish-3x1.png")
	fish4x2 = love.graphics.newImage("/assets/fish-4x2.png")

  love.window.setTitle("Fish Fillets remake")
love.graphics.setBackgroundColor(0.146, 0.73, 0.73)

local pb = pushblocks()			-- game logic
	pb:load (level)
end

function loadlevel()
local level = {}

level.name = 'level-1'

-- first cell is map[1][1], top left corner
level.map = 
{ -- 0 is empty, 1 is full,
 -- 2 an higher are empty, but you can use it as placeholders
 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,1,0,0,0,1,1}, 
    {1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,1,0,0,0,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,3,3,3,3,0,3,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,3,3,3,3,0,0,0,0,0,3,3,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,3,3,3,1,1,1,1,1,1,1,1},
    {1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,1,1},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
    {1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
    {1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
    {1,1,1,1,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,3,3},
    {1,1,1,1,1,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,1},
    {1,1,1,1,1,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
}

level.blocks = 
{
 {name = 'chair1',
 heavy = false,
 x = 8,
 y = 18,
 form = 
  {
   {1,0,0,0},
   {1,0,0,0},
   {1,1,1,1},
   {1,0,0,1},
   {1,0,0,1},
  },
 },
 
 {name = 'table1',
 heavy = false,
 x = 13,
 y = 19,
 form = 
  {
  {1,1,1,1,1,1,1},
  {0,1,0,0,0,1,0},
  {0,1,0,0,0,1,0},
  {0,1,0,0,0,1,0},
  },
 },
 
 {name = 'chair2',
 heavy = false,
 x = 21,
 y = 18,
 form = 
  {
   {0,0,0,1},
   {0,0,0,1},
   {1,1,1,1},
   {1,0,0,1},
   {1,0,0,1},
  },
 },
 
 {name = 'steel-pipe-1x81',
 heavy = true,
 x = 16,
 y = 1,
 form = 
  {
   {1},
   {1},
   {1},
   {1},
   {1},
   {1},
   {1},
   {1},
  },
 },


 {name = 'cushion1',
 heavy = false,
 x = 9,
 y = 19,
 form = 
  {
  {1,1,1},
  },
 },
}

level.agents = 
{
 {name = 'fish-3x1',
 fish = true,
 heavy = false,
 x = 13,
 y = 15,
 form = 
  {
   {1,1,1},
  },
 },
 
 
 {name = 'fish-4x2',
 fish = true,
 heavy = true,
 x = 20,
 y = 15,
 form = 
  {
   {1,1,1,1},
   {1,1,1,1},
  },
 },
}


-- prepare level

-- prepare map

level.w = 0 -- map width in tiles (same as highest x)
level.h = #level.map -- map height in tiles
for y, xs in ipairs (level.map) do
 for x, value in ipairs (xs) do
  if value == 1 then
   -- now value is true
   level.map[y][x] = true
   if level.w < x then level.w = x end
  elseif value==0 then
   -- now value is false
   level.map[y][x] = false
  elseif value==2 then
  level.map[y][x] = "exit"
  elseif value==3 then
  level.map[y][x] = "border"
  end
 end
end

for i, block in ipairs (level.blocks) do
 local w, h = 0, 0
 block.tiles = {}
 for y, xs in ipairs (block.form) do
  for x, value in ipairs (xs) do
   if value == 1 then
    table.insert (block.tiles, x-1) -- beware of -1
    table.insert (block.tiles, y-1)
    if w < x then w = x end
    if h < y then h = y end
   end
  end
 end
 block.w = w
 block.h = h
end

for i, agent in ipairs (level.agents) do
 local w, h = 0, 0
 agent.tiles = {}
 for y, xs in ipairs (agent.form) do
  for x, value in ipairs (xs) do
   if value == 1 then
    table.insert (agent.tiles, x-1) -- beware of -1
    table.insert (agent.tiles, y-1)
    if w < x then w = x end
    if h < y then h = y end
   end
  end
 end
 agent.w = w
 agent.h = h
 if not agent.direction then
  agent.direction = "right"
--  agent.direction = "left"
 end
end


return level
end

function pushblocks()

-- License CC0 (Creative Commons license) (c) darkfrei, 2022
-- push-blocks

local path = 'assets/'

pb = {}

local function setBlockOutline (block)
	local m = {} -- map of outlines
	local tiles = block.tiles -- list of tiles as {x1,y1, x2,y2, x3,y3 ...}
	for i = 1, #tiles, 2 do
		local x, y = tiles[i], tiles[i+1]
		if not m[y] then m[y] = {} end
		if not m[y][x] then 
			m[y][x] = {v=true, h=true} 
		else
			m[y][x].v = not m[y][x].v
			m[y][x].h = not m[y][x].h
		end
		
		if not m[y][x+1] then 
			m[y][x+1] = {v=true, h=false} 
		else
			m[y][x+1].v = not m[y][x+1].v
		end
		
		if not m[y+1] then m[y+1] = {} end
		if not m[y+1][x] then 
			m[y+1][x] = {v=false, h=true} 
		else
			m[y+1][x].h = not m[y+1][x].h
		end
	end
	local lines = {}
	for y, xs in pairs (m) do
		for x, tabl in pairs (xs) do
			if m[y][x].v then
				table.insert (lines, {x,y, x,y+1})
			end
			if m[y][x].h then
				table.insert (lines, {x,y, x+1,y})
			end
		end
	end
	block.lines = lines
end

function pb:load (level)
--	print (level.name)
	local width, height = love.graphics.getDimensions()
	self.map = level.map
	self.gridWidth = level.w
	self.gridHeight = level.h
	self.gridSize = math.min(width/(level.w), height/(level.h))
	--[[print ('gridWidth: ' .. self.gridWidth, 
		'gridHeight: ' .. self.gridHeight,
		'gridSize: ' .. self.gridSize)--]]
	
	self.blocks = level.blocks
	for i, block in ipairs (self.blocks) do
		setBlockOutline (block)
	end
	self.agents = level.agents
	for i, agent in ipairs (self.agents) do
		setBlockOutline (agent)
		local filename = path..agent.name..'.png'
		local info = love.filesystem.getInfo(filename)
		if info then
			local image = love.graphics.newImage(filename)
			agent.image = image
			local width, height = image:getDimensions( )
			local scale = self.gridSize/(width/agent.w)
			
			agent.image_scale = scale
			
			agent.image_sx = scale
			agent.image_sy = scale
			agent.image_ox = image:getWidth()/2
			agent.image_oy = image:getHeight()/2
			agent.image_dx = agent.image_ox*agent.image_scale
			agent.image_dy = agent.image_oy*agent.image_scale
		end
	end
	self.activeAgentIndex = 1
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true

		
end

function pb:switchAgent ()
	self.agent.active = false
	local index = self.activeAgentIndex + 1
	if index > #self.agents then
		index = 1
	end
	self.activeAgentIndex = index
	self.agent = self.agents[self.activeAgentIndex]
	self.agent.active = true
end


local function isValueInList (value, list)
	for i, element in ipairs (list) do
		if element == value then return true end
	end
	return false
end

function pb:isBlockToMapCollision (block, dx, dy)
	local x, y = block.x, block.y
	local map = self.map
	for i = 1, #block.tiles-1, 2 do
		local mapX = x + block.tiles[i]   + dx
		local mapY = y + block.tiles[i+1] + dy
		if map[mapY][mapX] then return true end
	end
end

function pb:isBlockToBlockCollision (blockA, blockB, dx, dy)
	-- fine tile to tile collision detection
	-- check if blockA moves to dx, dy an collides with blockB
	local xA, yA = blockA.x + dx, blockA.y + dy
	local xB, yB = blockB.x, blockB.y
	local tilesA = blockA.tiles
	local tilesB = blockB.tiles
	for i = 1, #tilesA-1, 2 do
		local dXA, dYA = tilesA[i], tilesA[i+1]
		for j = 1, #tilesB-1, 2 do
			local dXB, dYB = tilesB[j], tilesB[j+1]
			if (xA+dXA == xB+dXB) and (yA+dYA == yB+dYB) then
				-- same x AND same y means collision
				return true
			end
		end
	end
	return false
end


--testing
function pb:getCollisionExit (Agent)
for i, agent in ipairs (self.agents) do
		if agent == self.agent then
			-- no collision detection with active agent
		elseif self:isCollisionAgentToAnyExitArea (agent) then
		 return true -- collision with exit
		end
	end
end
	
function pb:getCollisionBlocks (blockA, blocks, dx, dy)
	-- agent to map or block to map collision
	if self:isBlockToMapCollision (blockA, dx, dy) then
		return false
	end
	
	for i, agent in ipairs (self.agents) do
		if agent == self.agent then
			-- no collision detection with active agent
		elseif self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return false -- cannot move any agent

		end
	end
	
	for i, block in ipairs (self.blocks) do
		if block == blockA then
			-- self collision: do nothing
		elseif isValueInList (block, blocks) then
			-- block is already in list: do nothing
		elseif self:isBlockToBlockCollision (blockA, block, dx, dy) then
			-- checks if the agent is strong
			if block.heavy and not self.agent.heavy then
				return false
			end
			table.insert (blocks, block)
			
			-- make it deeper!
			if not self:getCollisionBlocks (block, blocks, dx, dy) then
				return false
			end
		end
	end
	return true
end



function pb:getBlocksToMove (agent, dx, dy)
	local blocks = {}
	local canMove = self:getCollisionBlocks (agent, blocks, dx, dy)
	return blocks, canMove
end


function pb:moveAgent (agent, dx, dy)
	self.agent.x = self.agent.x + dx
	self.agent.y = self.agent.y + dy
end

function pb:moveBlocks (blocks, dx, dy)
	for i, block in ipairs (blocks) do
		block.x = block.x + dx
		block.y = block.y + dy
		if soundon==true then
		    scrape6:play()		--friction sound
		end
	end
end

function pb:isCollisionBlockToAllBlocks (blockA, dx, dy)
	for i, block in ipairs (self.blocks) do
		if not (block == blockA) 
		and self:isBlockToBlockCollision (blockA, block, dx, dy) then
			return true
		end
	end
	return false
end

function pb:isCollisionBlockToAllAgents (blockA, dx, dy)
	for i, agent in ipairs (self.agents) do
		if self:isBlockToBlockCollision (blockA, agent, dx, dy) then
			return agent -- dead agent :(
		end
	end
	return false
end

function pb:areAllAgentsInExitAreas ()
	for i, agent in ipairs (self.agents) do
		if not self:isCollisionAgentToAnyExitArea (agent) then
			return false -- at least one of agents is not on exit, the game is not over
		end
	return true -- all agents are in exit areas, level is done
	end
end

function pb:isCollisionAgentToAnyExitArea (agent)
	for i, area in ipairs (self.exitAreas) do
		if self:isBlockToBlockCollision (area, agent, 0, 0) then -- dx and dy are 0
			return true -- collision one of areas to agent in /this/ position
		end
	end
	return false -- this agent has no collision with any of exit areas
end

function pb:fallBlocks (blocks)
	local dx, dy = 0, 1 -- no horizontal speed, but positive (down) vertical
	for i = 1, self.gridWidth do
		for i, block in ipairs (blocks) do
			if self:isBlockToMapCollision (block, dx, dy) then
				-- not falling
				block.deadly = false
--				table.remove (blocks, i)
			elseif self:isCollisionBlockToAllBlocks (block, dx, dy) then
				block.deadly = false
				-- collision to block: probably not falling
			elseif block.deadly and self:isCollisionBlockToAllAgents (block, dx, dy) then
				local deadAgent = self:isCollisionBlockToAllAgents (block, dx, dy)
				deadAgent.dead = true
				block.deadly = false
--				table.remove (blocks, i)
			elseif self:isCollisionBlockToAllAgents (block, dx, dy) then
				-- the block is on fish
			else
				-- sure falling
				if soundon==true then
				    impactmetal:play()		--impact sound
				end
				block.x = block.x + dx -- never changes
				block.y = block.y + dy
				block.deadly = true
			end
		end
	end
end

function pb:mainMoving (dx, dy)
	local agent = self.agent -- active agent
	if dx > 0 then 
		agent.direction = "right"
	elseif dx < 0 then
		agent.direction = "left"
	end
	local blocks, canMove = self:getBlocksToMove (agent, dx, dy)
	if canMove then
		self:moveAgent (agent, dx, dy)
		self:moveBlocks (blocks, dx, dy)
		self:fallBlocks (self.blocks, dx, dy)
	end
end


function pb:keypressedMoving (scancode)
	if scancode == 'w' or scancode == 'a' or scancode == 's' or scancode == 'd' then
		-- d means 1; a means -1; otherwise 0
		local dx = scancode == 'd' and 1 or scancode == 'a' and -1 or 0
		-- s means 1; w means -1; otherwise 0
		local dy = scancode == 's' and 1 or scancode == 'w' and -1 or 0
		pb:mainMoving (dx, dy)
	end
	if scancode == 'right' or scancode == 'left' or scancode == 'up' or scancode == 'down' then
		local dx = scancode == 'right' and 1 or scancode == 'left' and -1 or 0
		local dy = scancode == 'down' and 1 or scancode == 'up' and -1 or 0
		pb:mainMoving (dx, dy)
	end
end

function love.gamepadpressed(joystick,button)

		if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
		
			if joystick:isGamepadDown("dpleft") then
				dx = -1
				dy = 0
			elseif joystick:isGamepadDown("dpright") then
				dx = 1
				dy = 0
			elseif  joystick:isGamepadDown("dpup") then
				dx = 0
				dy = -1
			elseif joystick:isGamepadDown("dpdown") then
				dx = 0
				dy = 1
			elseif joystick:isGamepadDown("a")  then
			pb:switchAgent ()
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("b")  then
			Talkies.clearMessages() talkies=false
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("y")  then
					if helpison=="yes" then helpison="no"
				elseif helpison=="no" then helpison="yes"
				end
			dx = 0
			dy = 0
			elseif joystick:isGamepadDown("start")  then
			dx = 0
			dy = 0
			gamestatus="levelselection"
			elseif joystick:isGamepadDown("back")  then
			dx = 0
			dy = 0
			shader2=false
			gamestatus="options"
			end
		    pb:mainMoving (dx, dy)
		end
		
		 if joystick:isGamepad() then
			isgamepad=true
		end
		
end


---------------------------------------------------------------------------------------------------
-- draw
---------------------------------------------------------------------------------------------------

function pb:drawBackgroundGrid ()
	local gridSize = self.gridSize
	local gridWidth = self.gridWidth
	local gridHeight = self.gridHeight
	love.graphics.setLineWidth(1)
	love.graphics.setColor(0.3,0.4,0.4)
	for i = 0, gridWidth do
		love.graphics.line (i*gridSize, 0, i*gridSize, gridHeight*gridSize)
					--line (i*gridSize, 0, i*gridSize, gridHeight*gridSize)
	end
	for i = 0, gridHeight do
		love.graphics.line (0, i*gridSize, gridWidth*gridSize, i*gridSize)
					--line (0, i*gridSize, gridWidth*gridSize, i*gridSize)
	end

end

function pb:drawMap ()
 local map = self.map
 local tileSize = self.gridSize
 love.graphics.setLineWidth(2)

 for y, xs in ipairs (map) do
  for x, value in ipairs (xs) do
   -- value is boolean: true or false
   if value==true then -- map tile
    -- beware of -1
      love.graphics.setColor(0,1,1)
      love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
							   --rect ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
    love.graphics.setColor(0,1,0)
      love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
							   --rect ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)

   elseif value=="exit" then -- map tile
   love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
							--line ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)

   --borders
   elseif value=="border" then -- map tile
    
                love.graphics.setColor(1,1,0)
		love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize)
								 --rect ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize) 
				love.graphics.setColor(1,0,0)
				love.graphics.rectangle ('line', (x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
					     --rect ((x-1)*tileSize, (y-1)*tileSize, tileSize, tileSize-15)
   end
  end
 end
 end
 
 
function pb:drawOutline  (block)

local lines = block.lines
 local tileSize = self.gridSize
  local x, y = block.x-1, block.y-1
   for i, line in ipairs (lines) do
   love.graphics.line ((x+line[1])*tileSize, (y+line[2])*tileSize, (x+line[3])*tileSize, (y+line[4])*tileSize)
			   --line ((x+line[1])*tileSize, (y+line[2])*tileSize, (x+line[3])*tileSize, (y+line[4])*tileSize)
   end
end    


function pb:drawBlock (block)

 local x, y = block.x, block.y
 local tileSize = self.gridSize
 for i = 1, #block.tiles-1, 2 do
  local dx, dy = block.tiles[i], block.tiles[i+1]
  -- beware of -1
  
  love.graphics.rectangle ('line', (x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
						   --rect ((x+dx-1)*tileSize, (y+dy-1)*tileSize, tileSize, tileSize)
 end
end


function pb:drawBlocks ()

 for i, block in ipairs (self.blocks) do
  -- draw filled block
  self:drawBlock (block)
  
  -- outline
  if block.heavy then
   love.graphics.setColor(0,1,1)
  else
   love.graphics.setColor(0,1,0)
  end
  self:drawOutline  (block)
 end
end

function pb:drawDeadAgent (agent)
	local tileSize = self.gridSize 
	local x = (agent.x-1)*tileSize
	local y = (agent.y-1)*tileSize
	local w = agent.w*tileSize
	local h = agent.h*tileSize
	
	love.graphics.line (x, y, x+w, y+h)
				--line (x, y, x+w, y+h)
	love.graphics.line (x, y+h, x+w, y)
				--line (x, y+h, x+w, y)
end


local function drawTexture (agent, tileSize)
 
 local sx = agent.image_sx
 if agent.direction and agent.direction == "left" then
  sx = -agent.image_sx
 end


  love.graphics.draw (agent.image, 
		(agent.x-1)*tileSize+agent.image_dx, 
		(agent.y-1)*tileSize+agent.image_dy,
		0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
  --img:draw((agent.x-1)*tileSize+agent.image_dx, 
  --(agent.y-1)*tileSize+agent.image_dy,
  --0, sx, agent.image_sy, agent.image_ox,  agent.image_oy)
  
end
local function drawagentrectangle(agent, tileSize)
 local sx = agent.image_sx
 love.graphics.rectangle ('fill',(agent.x-3)*tileSize+agent.image_dx, (agent.y-2)*tileSize+agent.image_dy, agent.image_dx*2, agent.image_dy*2)
						  --rect ((agent.x-3)*tileSize+agent.image_dx, (agent.y-2)*tileSize+agent.image_dy, agent.image_dx*2, agent.image_dy*2)
end                         


function pb:drawAgents ()

 local activeAgent = self.agent
 local tileSize = self.gridSize
 gridSize=self.gridSize
 for i, agent in ipairs (self.agents) do

  if agent.image then
   if agent == activeAgent then
    love.graphics.setColor(1,1,1)
  
    drawTexture (agent, tileSize)
   else
    love.graphics.setColor(0.7,0.7,0.7)
    
    drawTexture (agent, tileSize)
   end
  else
   if agent == activeAgent then
    love.graphics.setColor(1,1,1)
    self:drawBlock (agent)
    
    local x, y = agent.x, agent.y
    love.graphics.setColor (0, 0, 0)
    print (agent.x..' '..agent.y, (agent.x-1)*tileSize, (agent.y-1)*tileSize)
   else
    love.graphics.setColor(0.75,0.75,0.5)
    self:drawBlock (agent)
   end
   
   -- outline
   
   if agent.heavy then
    love.graphics.setColor(0,1,1)
   else
	love.graphics.setColor(0,1,0)
   end
   self:drawOutline  (agent)
  end

  if agent.dead then
   love.graphics.setColor(0,0,0)
   self:drawDeadAgent (agent)
  end
end
end

return pb

end

function love.gamepadpressed(joystick,button)

  if joystick:isGamepadDown("dpleft") or ("dpright") or ("dpup") or ("dpdown") then
  
   if joystick:isGamepadDown("dpleft") then
    dx = -1
    dy = 0
   elseif joystick:isGamepadDown("dpright") then
    dx = 1
    dy = 0
   elseif  joystick:isGamepadDown("dpup") then
    dx = 0
    dy = -1
   elseif joystick:isGamepadDown("dpdown") then
    dx = 0
    dy = 1
   elseif joystick:isGamepadDown("a")  then
   pb:switchAgent ()
   dx = 0
   dy = 0
   elseif joystick:isGamepadDown("b")  then
   Talkies.clearMessages() talkies=false
   dx = 0
   dy = 0
   elseif joystick:isGamepadDown("y")  then
     if helpison=="yes" then helpison="no"
    elseif helpison=="no" then helpison="yes"
    end
   dx = 0
   dy = 0
   elseif joystick:isGamepadDown("start")  then
   dx = 0
   dy = 0
   gamestatus="levelselection"
   elseif joystick:isGamepadDown("back")  then
   dx = 0
   dy = 0
   shader2=false
   gamestatus="options"
   end
      pb:mainMoving (dx, dy)
  end
  
   if joystick:isGamepad() then
   isgamepad=true
  end
end

function love.update(dt)
--[[local JOY_UP = love.input.joypad("up")
	local JOY_DOWN = love.input.joypad("down")
	local JOY_LEFT = love.input.joypad("left")
	local JOY_RIGHT = love.input.joypad("right")

	if JOY_UP    == 1 then local dy=dy-1 end
	if JOY_DOWN  == 1 then local dy=dy+1 end
	if JOY_LEFT  == 1 then local dx=dx-1 end
	if JOY_RIGHT == 1 then local dx=dx+1 end
	--]]
	pb:mainMoving (dx, dy)
end

function love.draw()
pb:drawMap ()
pb:drawBlocks ()
pb:drawAgents ()
pb:drawBackgroundGrid()
end
